#!/bin/bash

## app args
export APP_MEMORY=512MB
export SERVICE_PORT=8761
export MGMT_SERVICE_PORT=8762

## launch args
export CONFIG_APPLICATION=micloudeureka
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=${CONFIG_APPLICATION}-dist # <-- no trailing slash here.
# jvm arguments added here
export JVM_OPTS="" #"-Dspring.profiles.active=production"
