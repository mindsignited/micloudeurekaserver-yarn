micloudconfigserver
===================

MI Cloud Service Discovery using Netflix's Eureka using YARN

* Please setup Hadoop and Yarn using this link, for development.
    `https://github.com/spring-projects/spring-xd/wiki/Hadoop-Installation`

Add this to the `${HADOOP_HOME}/etc/hadoop/yarn-site.xml` for development only.

    <property>
        <name>yarn.log-aggregation-enable</name>
        <value>true</value>
    </property>
    <property>
         <name>yarn.scheduler.minimum-allocation-mb</name>
         <value>256</value>
    </property>
    <property>
        <name>yarn.nodemanager.delete.debug-delay-sec</name>
        <value>3600</value>
    </property>

Update this file `${HADOOP_HOME}/etc/hadoop/capacity-scheduler.xml` for development, as you will only be able to use 10% of
your system resources to run the cluster.  Increase the value by one, as needed.  If an application sits in an ACCEPTED state
for too long, your probably at the limit of what you can reserve.

    <property>
    <name>yarn.scheduler.capacity.maximum-am-resource-percent</name>
    <value>0.1</value>
    <description>
      Maximum percent of resources in the cluster which can be used to run
      application masters i.e. controls number of concurrent running
      applications.
    </description>
    </property>



* You must have Gradle, using 2.2.1 right now, installed locally.

* You'll need to have keycloak 1.1.0.Final running locally, also import the micloud-realm.json file. For Security.

* To build a distribution you just run `./gradlew clean build` - the artifacts will be in `${projectName}-dist/`.

* You can then run `./serverBoot.sh` to get it running on yarn locally.

* If it works you can run `yarn application -list` to see if `miclouyarntempalte` is in the RUNNING state, give it several minutes
 in RUNNING state to ensure it started successfully - or monitor the logs.



* NOTE: If you have YARN/Hadoop failures - that talk about not finding /bin/java - do this.

    `sudo ln -s <path-to-your-java> /bin/java`

- Need to figure out how to best debug these sorts of applications.
