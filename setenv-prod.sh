#!/bin/bash

##
## Environment settings - they are all defaults and not needed but are available.
##

## app args
export APP_MEMORY=1g
export SERVICE_PORT=8761
export MGMT_SERVICE_PORT=8762
export HADOOP_HOSTNAME=localhost
export HADOOP_HOST_PORT=8020
export EUREKA_PEERONE_HOSTNAME=discovery.mthinx.com
export EUREKA_PEERTWO_HOSTNAME=discovery-peer.mthinx.com


##
## JUST LAUNCHER ARGS - only needed for the init.d script.
##

## launch args
export CONFIG_APPLICATION=micloudeureka
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=/home/ubuntu/${CONFIG_APPLICATION} # <-- no trailing slash here.
# jvm arguments added here

export JVM_OPTS="-server -Xmx1536M -Xms1536M -XX:+UseConcMarkSweepGC -XX:+AggressiveOpts "
export SPRING_OPTS="--spring.profiles.active=production,peer1"
