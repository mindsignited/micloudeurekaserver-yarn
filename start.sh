#!/bin/bash

## MI Cloud Configuration Server
set -e

source ./setenv.sh

if [ "$1" == "debug" ]; then
   ./gradlew -PjvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5006" ${CONFIG_APPLICATION}-client:clean ${CONFIG_APPLICATION}-client:bootRun --debug-jvm
else
   ./gradlew ${CONFIG_APPLICATION}-client:clean ${CONFIG_APPLICATION}-client:bootRun
fi
