#!/bin/bash

## 
## Startup script for Eureka Server.  This will set the needed environment variables and
## boot up the application using the spring boot fat jar that is the artifact of the
## production build.
##
set -e

TO_RUN=client-*
PEER=peer1
ENV=
if [[ "x$1" == "xfatJar" || "x$2" == "xfatJar" || "x$3" == "xfatJar" || "x$4" == "xfatJar" ]]; then
    TO_RUN=container-*RELEASE
fi

if [[ "x$1" == "xpeer2" || "x$2" == "xpeer2" || "x$3" == "xpeer2" || "x$4" == "xpeer2" ]]; then
    ENV="--spring.profiles.active=production,peer2"
fi

if [[ "x$1" == "xpeer1" || "x$2" == "xpeer1" || "x$3" == "xpeer1" || "x$4" == "xpeer1" ]]; then
    ENV="--spring.profiles.active=production,peer1"
fi

if [ ! -d /opt/log/${CONFIG_APPLICATION} ]; then
     mkdir -p /opt/log/${CONFIG_APPLICATION}
     touch /opt/log/${CONFIG_APPLICATION}/${CONFIG_APPLICATION}.log
fi

## launch args
export CONFIG_APPLICATION=discovery
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=${CONFIG_APPLICATION}-dist # <-- no trailing slash here.
JVM_OPTS=""

if [[ "x$1" == "xdebug" || "x$2" == "xdebug" || "x$3" == "xdebug" || "x$4" == "xdebug" ]]; then
    JVM_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006"
fi

java ${JVM_OPTS} -jar ${PATH_TO_EXECUTABLE}/${CONFIG_APPLICATION}-${TO_RUN}.${CONFIG_APPLICATION_EXTENSION} ${ENV}
