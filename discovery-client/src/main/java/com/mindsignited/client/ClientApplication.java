package com.mindsignited.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.yarn.client.YarnClient;

@SpringBootApplication
public class ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args)
                .getBean(YarnClient.class)
                .submitApplication();
        ;
    }

}